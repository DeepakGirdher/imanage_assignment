'use strict';

angular.module('myApp.documents', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/documents', {
    templateUrl: 'documents/documents.html',
    controller: 'DocumentsCtrl'
  });
}])

.controller('DocumentsCtrl', ['$scope','$http','dataModel','$mdDialog', function($scope,$http,dataModel,$mdDialog) {
   
   /* Initialize the variables here */
   $scope.listLoaded = false;
   $scope.currentIndex = 0;  
   var tempData = [];

   /* Method to load the first 10 documents in the view */
   $scope.docInit = function() {
        for(var i=1; i<=10 ; i++){
          tempData.push(dataModel.projectData[$scope.currentIndex++])
        }
        $scope.listData = tempData;
        tempData = [];
        $scope.listLoaded = true;
   }


   /* Method to add more records once the user reaches the bottom of the page */
    $scope.add = function (){
      tempData = [];
        for(var i=1; i<=5 ; i++){
          if($scope.currentIndex == dataModel.projectData.length)
          {
            if(tempData.length > 0){
              return tempData;
            } else {
            return 'Finished';
            }
          }
          tempData.push(dataModel.projectData[$scope.currentIndex++])
        }
       return tempData;
      }



    /* Change the status of the selected document(s) when user clicks the delete button */
      $scope.deleteCallback = function(){
          for(var i=0; i<$scope.listData.length; i++){
              if($scope.listData[i].selected){
                  $scope.listData[i].status='trash';
                  dataModel.projectData[i].status='trash';
                  $scope.listData[i].selected = false;
              }
          }
      }

      /* Change the name of the document(s) when user selects and clicks the rename button.
         fetch the new name using the dialog box */
         $scope.renameCallback = function(){
              var confirm = $mdDialog.prompt()
                .title('Enter the new name for selected document(s)')
                .textContent('Enter the name in numerics only.')
                .placeholder('111')
                .ariaLabel('111')
                .initialValue('111')
                .ok('Rename')
                .cancel('Cancel');

              $mdDialog.show(confirm).then(function(result) {
                     for(var i=0; i<$scope.listData.length; i++){
                          if($scope.listData[i].selected){
                              $scope.listData[i].docNumber=result;
                              $scope.listData[i].selected = false;
                          }
                      }               
              }, function() {
                
              });
         }

      /* Initate the view data by calling this method */
      $scope.docInit(); 
}]);  