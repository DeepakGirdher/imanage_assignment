'use strict';

/* Declare app level module which depends on views, and components */
angular.module('myApp', [
  'ngRoute',
  'myApp.documents',
  'myApp.trash',
  'myApp.listBox',
  'ngMaterial'
])

.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');
  $routeProvider.otherwise({redirectTo: '/documents'});
}])

/* Service to hold the data throughout the project lifecycle. */
.service('dataModel',['$http', function($http){

    var self=this;
    self.projectData = [];
    
    /* Function to fetch the data from the json file and store in the service. */
    self.initProject = function(){
        $http.get('dummyData/documents.json').then(function(result){
        self.projectData = result.data;
        },function(err){
                console.log(err);
        });
    }

}])

/* Load the dummy data to projectData service for usage in the project. */
.run(['dataModel',function(dataModel){
    dataModel.initProject();  
}])

.controller('menuController',['$scope','$location',function($scope,$location){
  $scope.isActive = function(viewLocation) {
    return viewLocation === $location.path();
  }
}]);
