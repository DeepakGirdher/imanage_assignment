'use strict';

angular.module('myApp.trash', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/trash', {
    templateUrl: 'trash/trash.html',
    controller: 'TrashCtrl'
  });
}])

.controller('TrashCtrl', ['$scope','dataModel',function($scope,dataModel) {

   /* Initialize the variables here */
   $scope.listLoaded = false;
   $scope.currentIndex = 0;  
   var tempData = [];

   /* Method to fetch first 10 documents from the projectData service and push to directive for display on view */
   $scope.docInit = function() {
        for(var i=1; i<=10 ; i++){
          tempData.push(dataModel.projectData[$scope.currentIndex++])
        }
        $scope.listData = tempData;
        tempData = [];
        $scope.listLoaded = true;
   }


   /* Method to show 5 more documents once the user reaches the bottom of the window */
    $scope.add = function (){
      tempData = [];
      if(dataModel.projectData.length<10){
        return 'Finished';
      }

        for(var i=1; i<=5 ; i++){
          if($scope.currentIndex == dataModel.projectData.length)
          {
            if(tempData.length > 0){
              return tempData;
            } else {
            return 'Finished';
            }
          }
          tempData.push(dataModel.projectData[$scope.currentIndex++])
        }
       return tempData;
      }


      /* Callback  function to delete the selected documents once user selects and click the delete button */
      $scope.deleteCallback = function(){
          for(var i=0; i<$scope.listData.length; i++){
              if($scope.listData[i].selected){
                  $scope.listData[i].status='deleted';
                  dataModel.projectData[i].status='deleted';
                  $scope.listData[i].selected = false;
              }
          }
      }

      /* Callback  function to restore the selected documents once user selects and click the restore  button */
      $scope.restoreCallback = function(){
          for(var i=0; i<$scope.listData.length; i++){
              if($scope.listData[i].selected){
                  $scope.listData[i].status='active';
                  dataModel.projectData[i].status='active';
                  $scope.listData[i].selected = false;
              }
          }
      }

      /* Function called to initialize the view with first time */
      $scope.docInit(); 

}]);