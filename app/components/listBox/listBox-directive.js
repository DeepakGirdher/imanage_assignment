'use strict';

angular.module('myApp.listBox.listBox-directive', [])

.directive('listBox', function($window) {
  return {
     restricted: 'E',
     scope: {
       'listData': '=',
       'status': '@',
       'buttonLabelOne': '@',
       'buttonLabelTwo': '@',
       'iconOne': '@',
       'iconTwo': '@',
       'loadData': '&',
       'buttonOneFunction': '&',
       'buttonTwoFunction': '&'
     },
    templateUrl: 'components/listBox/listBox-template.html',
    link: function(scope, element, attrs) {
      
      /* Variable initiation to set up the directive page */
      scope.allSelected = false;
      scope.loadMore = true;
      scope.noMoreRecords = false;
      var raw=element[0];

      /* This function will be called when the bottom of the window is reached on scroll. */
      scope.more = function(){
         var temp = scope.loadData();
         if (temp == 'Finished'){
             scope.loadMore = false;
             scope.noMoreRecords = true;
         }else {
            for(var i=0;i<temp.length;i++){
                scope.listData.push(temp[i]);
            }
         }
         
       }

       
       /* This method will be called when user selects or deselects the checkbox in the header 
          It will either select or deselect all the visible records based on selection. */
       scope.selectAll = function(){
         if(scope.allSelected){
              for(var i=0;i<scope.listData.length;i++){
                  scope.listData[i].selected = false;
              }
         }else {
           for(var i=0;i<scope.listData.length;i++){
                  scope.listData[i].selected = true;
              }
         }
       }

       /* Callback function called on the click of First Button in the toolbar */
       scope.buttonOne = function(){
          scope.buttonOneFunction();
       }

       /* Callback function called on the click of First Button in the toolbar */
       scope.buttonTwo = function(){
          scope.buttonTwoFunction();
       }

      /* Logic to figure out when the bottom of the window has been reached while scrolling */
      angular.element($window).bind("scroll",function(){
        var windowHeight = 'innerHeight' in window ? window.innerHeight : document.offsetHeight;
        var body = document.body, html = document.documentElement;
        var docHeight = Math.max(body.scrollHeight, body.offsetHeight,html.clientHeight,html.scrollHeight,html.offsetHeight);
        var windowBottom = windowHeight + window.pageYOffset;
        if (windowBottom >= docHeight) {
          scope.$apply(function(){
                scope.more();
          });
        }
      });
       
     }

  };
});