# How to setup and execute the application
1.      git clone https://DeepakGirdher@bitbucket.org/DeepakGirdher/imanage_assignment.git
1. 	npm install
1. 	bower install (if asked to select angular version select 1.6.3
1. 	npm start
1. 	Go to http://localhost:8000 in your browser

(Internet is required while running the application as icons are fetched using material icons google link)

# Implementation details
The application has been made with AngularJS version 1. Angular Material is used along with some custom css to achieve the look and feel. Angular seed was used for the initial setup of the project.

Two views created for Documents and Trash and a reusable directive created for showing the document list. Based on the actions taken by the user i.e. Delete or Restore, callback function was provided to the controllers.

A service was used to hold the data during the project for the particular session. All controllers are referring to this cached data for actions. The dummy data is refreshed back once application is restarted.

# Tasks completed
1. SPA created using AngularJs.
1. Toolbar and Side navigation created.
1. Load on scroll (lazy load) implemented with 10 documents shown first time and 5 documents added every time user reaches the window bottom.
1. Selecting the checkbox in header selects/de-selects all the documents.
1. Delete button in documents view is moving the documents to trash view.
1. Delete button in trash view is deleting the documents.
1. Rename button is changing the document number of selected documents.


# Tasks pending
1. Implementation of Search functionality.
1. Confirmation box while deleting documents.
1. The floating menu coming near the document list to Rename and Delete.

# Known issues
1. If number of records are less than 10 in the view, the bottom message of scroll changes only after scroll is brought up and taken back to bottom 3 times.
1. The active menu on the side navigation bar is not changing to white color and slightly moved

# Areas of improvement
1. Spinners to be added while fetching and displaying the data.
2. Fonts to be applied to make it look better.
3. Better data model and structure can be implemented to improve the performance of the application.
4. Validation and better design of rename dialog box.
5. Error Handling and Showing Toaster message to the users.
6. Unit Test Cases.